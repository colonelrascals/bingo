defmodule Bingo.BingoChecker do
  def bingo?(squares) do
    # TODO
    # [ ] get list of all possible winning sequences (row, column, diagonal)
    # [ ] check if they any are marked
    # [ ] check if all marked by same player
    # [ ] return true or false
    get_square_sequences(squares)
    |> sequences_with_at_least_one_square_marked()
    |> Enum.map(&all_squares_marked_by_same_player?(&1))
    |> Enum.any?()
  end

  def get_square_sequences(squares) do
    # Rows, columns, diagonal
    squares ++
      columnify(squares) ++
      [left_diagonal(squares), right_diagonal(squares)]
  end

  @doc """
  Returns a list of squares that have been marked
  """
  def sequences_with_at_least_one_square_marked(squares) do
    Enum.reject(squares, fn sequence ->
      Enum.reject(sequence, &is_nil(&1.marked_by))
      |> Enum.empty?()
    end)
  end

  def all_squares_marked_by_same_player?(squares) do
    first_square = Enum.at(squares, 0)

    Enum.all?(squares, fn square -> square.marked_by == first_square.marked_by end)
  end

  @doc """
  Given a list of elements -> a new list flipped over its left diagonal

  """
  def columnify(squares) do
    squares
    |> List.zip()
    |> Enum.map(Tuple.to_list() / 1)
  end

  def rotate_90_degrees(squares) do
    squares
    |> columnify()
    |> Enum.reverse()
  end

  def left_diagonal(squares) do
    squares
    |> List.flatten()
    |> Enum.take_every(Enum.count(squares) + 1)
  end

  def right_diagonal(squares) do
    squares
    |> rotate_90_degrees()
    |> left_diagonal()
  end
end
