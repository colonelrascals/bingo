defmodule Bingo.Game do
  defstruct squares: nil, scores: %{}, winner: nil

  alias Bingo.{Buzzwords, Square, Game, BingoChecker}

  def new(size) when is_integer(size) do
    buzzwords = Buzzwords.read_buzzwords()
    IO.puts(buzzwords)
    Game.new(size, buzzwords)
  end

  def new(size, buzzwords) do
    squares =
      buzzwords
      |> Enum.shuffle()
      |> Enum.take(size * size)
      |> Enum.map(&Square.from_buzzwords(&1))
      |> Enum.chunk_every(size)

    %Game{squares: squares}
  end

  def mark(game, phrase, player) do
    game
    |> mark_squares(phrase, player)
    |> update_scores
    |> assign_winner(player)
  end

  def mark_squares(game, phrase, player) do
    new_squares =
      game.squares
      |> List.flatten()
      |> Enum.map(&mark_square(&1, phrase, player))
      |> Enum.chunk_every(Enum.count(game.squares))

    %{game | squares: new_squares}
  end

  def mark_square(square, phrase, player) do
    case square.phrase == phrase do
      true -> %Square{square | marked_by: player}
      false -> square
    end
  end

  def update_scores(game) do
    scores =
      game.scores
      |> List.flatten()
      |> Enum.reject(&is_nil(&1.marked_by))
      |> Enum.map(fn sq -> {sq.marked_by.name, sq.points} end)
      |> Enum.reduce(%{}, fn {name, points}, scores ->
        Map.update(scores, name, points, &(&1 + points))
      end)

    %{game | scores: scores}
  end

  def assign_winner(game, player) do
    case BingoChecker.bingo?(game) do
      true -> %{game | winner: player}
      false -> game
    end
  end
end
