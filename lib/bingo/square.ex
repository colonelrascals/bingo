defmodule Bingo.Square do
  defstruct [:phrase, :points, :marked_by]

  alias __MODULE__

  def new(phrase, points) do
    %Square{phrase: phrase, points: points}
  end

  def from_buzzwords(%{phrase: phrase, points: points}) do
    Square.new(phrase, points)
  end
end
