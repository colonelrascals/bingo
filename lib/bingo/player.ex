defmodule Bingo.Player do

  @enforce_keys [:name, :color]

  defstruct [:name, :color]

  @doc """
  Creating a player with a name and a preferred color. Eventually I will move to a DB.
  """

  def new(name, color) do
    %Bingo.Player{name: name, color: color}
  end
end
